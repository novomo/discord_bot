const { TwitterApi } = require("twitter-api-v2");

module.exports = async (msg, img = "") => {
  console.log(msg);
  if (msg === "" || !msg) {
    return;
  }
  console.log(process.env.TWITTER_CONSUMER);
  const client = new TwitterApi({
    appKey: process.env.TWITTER_CONSUMER,
    appSecret: process.env.TWITTER_CONSUMER_SECRET,
    accessToken: process.env.TWITTER_ACCESS_TOKEN,
    accessSecret: process.env.TWITTER_ACCESS_TOKEN_SECRET,
  });
  const bearer = new TwitterApi(process.env.TWITTER_BEARER);

  const twitterClient = client.readWrite;
  //const twitterBearer = bearer.readOnly;

  try {
    if (img === "") {
      const tweet = await twitterClient.v2.tweet({
        text: `${msg}

        #GamblingTwitter #sportsbettingtwitter
        #MachineLearning
        `,
      });
      console.log(tweet);
    } else {
      const mediaId = await twitterClient.v1.uploadMedia(img);
      await twitterClient.v2.tweet({
        text: `${msg}

        #GamblingTwitter #sportsbettingtwitter
        #MachineLearning
        `,
        media: {
          media_ids: [mediaId],
        },
      });
    }
  } catch (e) {
    console.log(e);
  }
};
