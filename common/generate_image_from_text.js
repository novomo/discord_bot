const { createCanvas, loadImage } = require("canvas");
const fs = require("fs");
module.exports = async (txt) => {
  const canvas = createCanvas(1080, 1080);
  const ctx = canvas.getContext("2d");
  const bgImage = await loadImage("./insta_bg.png");

  // center fill
  const hRatio = canvas.width / bgImage.width;
  const vRatio = canvas.height / bgImage.height;
  const ratio = Math.max(hRatio, vRatio);
  const centerShift_x = (canvas.width - bgImage.width * ratio) / 2;
  const centerShift_y = (canvas.height - bgImage.height * ratio) / 2;

  ctx.drawImage(
    bgImage,
    0,
    0,
    bgImage.width,
    bgImage.height,
    centerShift_x,
    centerShift_y,
    bgImage.width * ratio,
    bgImage.height * ratio
  );

  //ctx.rect(0, 0, canvas.width, canvas.height);
  //ctx.fillStyle = "#000000aa";
  //ctx.fill();

  //const posterImage = await loadImage(
  //"https://firebasestorage.googleapis.com/v0/b/centered-staging-groups/o/p21O3WRts9SjDIeaGjJP2gaPb7n1-qa-test-cassidy-feature.jpg?alt=media&token=433f5479-549e-4bd1-9543-61879de08c8c"
  //);

  //ctx.save();

  //ctx.clip();

  //ctx.drawImage(posterImage, 814, 51, 336, 528);

  ctx.restore();

  //const logoImage = await loadImage("./logo-trans.png");

  //ctx.drawImage(logoImage, 121, 252, 462, 114);
  let str = txt.trim();
  str = str.replace("action", "\naction");
  str = str.replace("@", "\n@");
  str = str.replace(
    str.split("\n@ ", 2)[1].split(" ", 2)[1],
    `\n${str.split("\n@ ", 2)[1].split(" ", 2)[1]}`
  );
  console.log(str);
  let text_height = ctx.measureText().height;
  let text_width = ctx.measureText().width;
  let x1 = 50,
    y1 = 400,
    x2,
    y2;
  x2 = x1 + text_width;
  y2 = y1 + text_height;

  console.log(text_width);
  ctx.rect(x1, y1, x2, y2);
  ctx.textBaseline = "bottom";
  ctx.textAlign = "start";
  ctx.fillStyle = "#9dedc0";
  if (text_width > 50) {
    ctx.font = "28px sans-serif";
  } else {
    ctx.font = "32px sans-serif";
  }

  ctx.fillText(str, x1, y1);

  const buffer = canvas.toBuffer("image/jpeg");
  fs.writeFileSync("instagram.jpg", buffer);
  return "instagram.png";
};
