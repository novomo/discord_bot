bot_url =
  "https://discord.com/api/oauth2/authorize?client_id=1129324130815180840&permissions=8&scope=applications.commands%20bot";
require("dotenv").config();



const { token } = process.env;
const { Client, Collection } = require("discord.js");
const fs = require("fs");

const client = new Client({ intents: 32767 });
client.commands = new Collection();
client.commandsArray = [];

const functionFolders = fs.readdirSync(`./functions`);
for (const folder of functionFolders) {
  console.log(folder);
  const functionFiles = fs
    .readdirSync(`./functions/${folder}`)
    .filter((file) => file.endsWith(".js"));
  for (const file of functionFiles) {
    console.log(file);
    require(`./functions/${folder}/${file}`)(client);
  }
}

client.events();
client.commands();
client.login(token);
