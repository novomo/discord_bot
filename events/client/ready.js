const IP = require("ip");
const upload_error = require("../../node_error_functions/upload_error");

module.exports = {
  name: "ready",
  once: true,
  async execute(client) {
    console.log(`Ready!!! ${client.user.tag} is logged in and online`);
    console.log(process.env.IG_PASSWORD);
    setInterval(async () => {
      try {
        console.log("now");
        await client.post_tip();
        await client.post_trade();
        await client.react_to_tip();

        /*if (count === 5) { // if it has been run 5 times, we resolve the promise
          clearInterval(interval);
          resolve(res); // result of promise
      }*/
      } catch (err) {
        console.log(err);
        upload_error({
          errorTitle: "Starting Discord Bot",
          machine: IP.address(),
          machineName: "API",
          errorFileName: __filename.slice(__dirname.length + 1),
          err: err,
          critical: true,
        });
      }
    }, 1000 * 60);
  },
};
