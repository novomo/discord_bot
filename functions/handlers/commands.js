const fs = require("fs");
const IP = require('ip');
const upload_error = require("../../node_error_functions/upload_error")
const { Routes } = require("discord-api-types/v9");
const { REST } = require("@discordjs/rest");
module.exports = (client) => {
  client.commands = async () => {
    const commandFolders = fs.readdirSync(`./commands`);
    const { commands, commandsArray } = client;
    for (const folder of commandFolders) {
      const commandFiles = fs
        .readdirSync(`./commands/${folder}`)
        .filter((file) => file.endsWith(".js"));

      for (const file of commandFiles) {
        const command = require(`../../commands/${folder}/${file}`)(client);
        commands.set(command.data.name, command);
        commandsArray.push(command, command.data.toJSON());
      }
    }
    const clientId = process.env.clientId;
    const guildId = process.env.guildId;
    const rest = new REST({ version: "9" }).setToken(process.env.token);
    try {
      console.log("Started refreshing apllications commands");

      await rest.put(Routes.applicationGuildCommands(clientId, guildId), {
        body: commandsArray,
      });
      console.log("succesfully loaded applications commands");
    } catch (err) {
      console.log(err)
      upload_error({
        errorTitle: "Loading Commands",
        machine: IP.address(),
        machineName: "API",
        errorFileName: __filename.slice(__dirname.length + 1),
        err: err,
        critical: true
      })
    }
  };
};
