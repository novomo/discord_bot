const fs = require("fs");
const IP = require('ip');
const upload_error = require("../../node_error_functions/upload_error")
module.exports = (client) => {
  client.events = async () => {
    try {
    const eventFolders = fs.readdirSync(`./events`);
    for (const folder of eventFolders) {
      const eventFiles = fs
        .readdirSync(`./events/${folder}`)
        .filter((file) => file.endsWith(".js"));

      switch (folder) {
        case "client":
          for (const file of eventFiles) {
            const event = require(`../../events/${folder}/${file}`);
            if (event.once)
              client.once(event.name, (...args) =>
                event.execute(...args, client)
              );
            else
              client.on(event.name, (...args) =>
                event.execute(...args, client)
              );
          }
          break;
        default:
          break;
      }
    }
  } catch (err) { 
    console.log(err)
      upload_error({
        errorTitle: "Loading Event",
        machine: IP.address(),
        machineName: "API",
        errorFileName: __filename.slice(__dirname.length + 1),
        err: err,
        critical: true
      })
  }
  };
};
