const mysqlx = require("@mysql/xdevapi");
const IP = require("ip");

const toTimestamp = (d) => {
  let dt;
  if (typeof d === "string") {
    dt = new Date(d);
  } else {
    dt = d;
  }
  //console.log(dt);
  return parseInt(dt / 1000);
};

const stratgies = ["Win", "E/W"];
const upload_error = require("../../node_error_functions/upload_error");
module.exports = async (client) => {
  client.post_trade = async () => {
    let sclient = mysqlx.getClient(
      `${process.env.MYSQL_USER}:${process.env.MYSQL_PASS}@${process.env.MYSQL_HOST}`
    );
    let mysqlClient = null;
    let mysqlDb = null;
    try {
      mysqlClient = await sclient.getSession();
    } catch (err) {
      await sclient.close();
      sclient = mysqlx.getClient(
        `${process.env.MYSQL_USER}:${process.env.MYSQL_PASS}@${process.env.MYSQL_HOST}`
      );

      mysqlClient = await sclient.getSession();
    }
    mysqlDb = await mysqlClient.getSchema(process.env.MYSQL_DATABASE);

    try {
      let sports_trade_collection = await mysqlDb.getCollection(
        "sports_trades"
      );

      let sports_trades = await sports_trade_collection
        .find(
          `newPosition = true AND userID = 0 and tradeDateTime > ${
            toTimestamp(new Date()) - 1000 * 3600
          }`
        )
        .execute();

      sports_trades = sports_trades.fetchAll();

      console.log(sports_trades);
      for (const sports_trade of sports_trades) {
        const channel = client.channels.cache.get(
          process.env.sportsTradesChannel
        );
        let newPositions = [];
        for (const position of sports_trade.positions) {
          if (position.messageId) {
            newPositions.push(position);
            continue;
          }
          console.log(position);
          let messageId;
          if (stratgies.includes(position.strategy)) {
            const msg = await channel.send(position.discordMsg);
            console.log(msg);
            messageId = msg.id;
          }

          newPositions.push({ ...position, messageId: messageId });
        }

        await sports_trade_collection
          .modify("_id like :tradeID")
          .bind("tradeID", sports_trade._id)
          .set("positions", newPositions)
          .set("newPosition", false)
          .execute();
      }
      await mysqlClient.close();
      await sclient.close();
      console.log("return");
      return;
    } catch (err) {
      console.log(err);
      await mysqlClient.close();
      await sclient.close();
      console.log("return");
      upload_error({
        errorTitle: "Posting Trade",
        machine: IP.address(),
        machineName: "API",
        errorFileName: __filename.slice(__dirname.length + 1),
        err: err,
        critical: true,
      });
      return;
    }
  };
};
