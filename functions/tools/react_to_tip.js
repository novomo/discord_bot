const mysqlx = require("@mysql/xdevapi");
const IP = require('ip');
const upload_error = require("../../node_error_functions/upload_error")
module.exports = async (client) => {
  client.react_to_tip = async () => {
    let sclient = mysqlx.getClient(
      `${process.env.MYSQL_USER}:${process.env.MYSQL_PASS}@${process.env.MYSQL_HOST}`
    );
    let mysqlClient = null;
    let mysqlDb = null;
    try {
      mysqlClient = await sclient.getSession();
    } catch (err) {
      await sclient.close();
      sclient = mysqlx.getClient(
        `${process.env.MYSQL_USER}:${process.env.MYSQL_PASS}@${process.env.MYSQL_HOST}`
      );

      mysqlClient = await sclient.getSession();
    }
    mysqlDb = await mysqlClient.getSchema(process.env.MYSQL_DATABASE);

    try {
      let tip_collection = await mysqlDb.getCollection("tips");

      let tips = await tip_collection
        .find(
          "action <> 'leave' and messageId <> '' and result <> '' and reacted = false"
        )
        .execute();

      tips = tips.fetchAll();
      for (const tip of tips) {
        const channel = client.channels.cache.get(process.env.tipsChannel);

        const m = await channel.messages.fetch(tip.messageId);
        if (tip.actionResult === "Win" || tip.actionResult === "Half Win") {
          m.react("💥");
        } else if (
          tip.actionResult === "Loss" ||
          tip.actionResult === "Half Loss"
        ) {
          m.react("🗑️");
        }
        if (tip.actionResult === "Void" || tip.actionResult === "Push") {
          m.react("✋");
        }

        await tip_collection
          .modify("_id like :tipID")
          .bind("tipID", tip._id)
          .patch({ reacted: true })
          .execute();
      }
      await mysqlClient.close();
      await sclient.close();
      console.log("return");
      return;
    } catch (err) {
      console.log(err);
      await mysqlClient.close();
      await sclient.close();
      console.log("return");
      upload_error({
        errorTitle: "Reacting to tip",
        machine: IP.address(),
        machineName: "API",
        errorFileName: __filename.slice(__dirname.length + 1),
        err: err,
        critical: true
      })
      return;
    }
  };
};
