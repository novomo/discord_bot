const mysqlx = require("@mysql/xdevapi");

const IP = require("ip");
const upload_error = require("../../node_error_functions/upload_error");
const tweet = require("../../common/twitter_post");
const insta_post = require("../../common/instagram_post");

module.exports = async (client) => {
  client.post_tip = async () => {
    let sclient = mysqlx.getClient(
      `${process.env.MYSQL_USER}:${process.env.MYSQL_PASS}@${process.env.MYSQL_HOST}`
    );
    let mysqlClient = null;
    let mysqlDb = null;
    try {
      mysqlClient = await sclient.getSession();
    } catch (err) {
      await sclient.close();
      sclient = mysqlx.getClient(
        `${process.env.MYSQL_USER}:${process.env.MYSQL_PASS}@${process.env.MYSQL_HOST}`
      );

      mysqlClient = await sclient.getSession();
    }
    mysqlDb = await mysqlClient.getSchema(process.env.MYSQL_DATABASE);

    try {
      let tip_collection = await mysqlDb.getCollection("tips");

      let tips = await tip_collection
        .find("messageId = '' and notificationId = ''")
        .execute();
      ("1125916287009312839");
      tips = tips.fetchAll();
      //console.log(tips);
      const tipsNotificationChannel = await client.channels.cache.get(
        process.env.tipsNotificationChannel
      );
      const tipsChannel = await client.channels.cache.get(
        process.env.tipsChannel
      );
      for (const tip of tips) {
        //console.log(client.channels.cache);

        //console.log(tipsNotificationChannel);

        const notification = await tipsNotificationChannel.send(tip.discordMsg);
        //console.log(notification);
        const notificationId = notification.id;

        await tip_collection
          .modify("_id like :tipID")
          .bind("tipID", tip._id)
          .patch({ notificationId: notificationId })
          .execute();
      }

      tips = await tip_collection
        .find(
          `messageId = '' and notificationId <> '' and tipDate > ${
            Math.floor(Date.now() / 1000) - 3600 * 4
          }`
        )
        .execute();
      ("1125916287009312839");
      tips = tips.fetchAll();

      for (const tip of tips) {
        let m;
        try {
          m = await tipsNotificationChannel.messages.fetch(tip.notificationId);
          //console.log(m);
          if (!m) {
            continue;
          }
        } catch (err) {
          continue;
        }

        const reactions = await m.reactions.cache;
        console.log(reactions.size);
        console.log(reactions[0]);
        if (reactions.size === 0) {
          continue;
        }

        reactions.forEach(async (reaction) => {
          const emojiName = reaction._emoji.name;
          const txtAction = tip.discordMsg.split("action: ")[1].split(" @")[0];
          let new_msg;
          let str = tip.discordMsg.trim();
          str = str.replace("@", "\n@");
          str = str.replace(
            str.split("\n@ ", 2)[1].split(" ", 2)[1],
            `\n${str.split("\n@ ", 2)[1].split(" ", 2)[1]}`
          );
          str = str.split("action: ")[0].trim();
          //console.log(tip);
          if (emojiName === "💥" || emojiName === "🗑️") {
            if (emojiName === "💥") {
              new_msg = `${str}
action: tail @ ${parseFloat(tip.originalOdds).toFixed(2)} for ${parseFloat(
                tip.units
              ).toFixed(2)} unit/s`;
            } else if (emojiName === "🗑️") {
              let o = tip.odds;
              if (tip.atcion !== "fade") {
                let impliedProb = (1 / tip.odds) * 100;
                let sideImplied = (100 - impliedProb) * 1.09;
                let fadeOdds = 100 / sideImplied;
                o = fadeOdds;
              }
              new_msg = `${str}
action: fade @ ${parseFloat(o).toFixed(2)} for ${parseFloat(tip.units).toFixed(
                2
              )} unit/s`;
            }
            // remove time from string

            const message = await tipsChannel.send(new_msg);
            //console.log(message);
            const messageId = message.id;
            await tweet(new_msg);
            //await insta_post(tip.discordMsg);
            try {
              await tip_collection
                .modify("_id like :tipID")
                .bind("tipID", tip._id)
                .patch({ messageId: messageId })
                .execute();
            } catch (err) {
              if (err.toString().includes("This session was closed.")) {
                await mysqlClient.close();
                mysqlClient = await sclient.getSession();

                mysqlDb = await mysqlClient.getSchema(
                  process.env.MYSQL_DATABASE
                );

                tip_collection = await mysqlDb.getCollection("tips");
                await tip_collection
                  .modify("_id like :tipID")
                  .bind("tipID", tip._id)
                  .patch({ messageId: messageId })
                  .execute();
              }
            }
          }
        });
      }

      await mysqlClient.close();
      await sclient.close();
      console.log("return");
      return;
    } catch (err) {
      console.log(err);
      await mysqlClient.close();
      await sclient.close();
      console.log("return");
      upload_error({
        errorTitle: "Posting Tip",
        machine: IP.address(),
        machineName: "API",
        errorFileName: __filename.slice(__dirname.length + 1),
        err: err,
        critical: true,
      });
      return;
    }
  };
};
